# Formats XML tests

# Variables
ROOT=$(dirname $0)/../

# Goes to root directory
cd $ROOT

# Formats each XML
for file in tests/*.xml; do
  xmllint -o $file --encode utf-8 --format $file
done
